﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class detectionflymob : MonoBehaviour
{ 
    public float speed;
    public Transform player;
    public GameObject monster;
    public bool followingPL;

    public float knockBackCount;
    public Rigidbody2D rgbd;
    
    // Start is called before the first frame update
    void Start()
    {
        rgbd = GetComponentInParent<Rigidbody2D>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {

            followingPL = true;
        }
    }

    public void OnTriggerExit2D(Collider2D other)
    {// Exit l'aggro du monstre et ducoup il reprend sa patrouille
        if (other.gameObject.CompareTag("Player"))
        {
           
            followingPL = false;
        }
    }
    // Update is called once per frame
    void Update()
    {
        knockBackCount = GetComponentInParent<monsterScript>().knockbackCount;
        
        Vector2 target = new Vector2(player.transform.position.x, player.position.y);
        if (knockBackCount <= 0)
        {
            rgbd.velocity = Vector2.zero;
            if (followingPL)
            {
                monster.transform.position =  Vector2.MoveTowards(transform.position, target,speed * Time.deltaTime);
            }
        }
    }
}
