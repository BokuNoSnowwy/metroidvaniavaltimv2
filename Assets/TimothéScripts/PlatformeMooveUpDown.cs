﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class PlatformeMooveUpDown : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Sequence mySequence = DOTween.Sequence();
        mySequence.Append(transform.DOMoveY(transform.position.y  -10, 4));
        mySequence.AppendCallback(UpDown);
    }

    public void UpDown()
    {
        Sequence mySequence1 = DOTween.Sequence();
        mySequence1.Append(transform.DOMoveY(transform.position.y  +10, 4));
        mySequence1.AppendCallback(Start);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
