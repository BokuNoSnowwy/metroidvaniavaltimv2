﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class heartrecup : MonoBehaviour
{
    public PlayerStats playerheart;
    public int healthpoint = 20;
    // Start is called before the first frame update
    void Start()
    {
        if (!playerheart)
        {
            playerheart = GameObject.FindObjectOfType<PlayerStats>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnCollisionEnter2D(Collision2D other)
    {// Permet au player de récupérer de la vie
        if (other.gameObject.CompareTag("Player"))
        {
            FindObjectOfType<Audiomanager>().Play("HeartRecup");
            playerheart.CurrentHealth += healthpoint;
            
            playerheart.healthbar.value = playerheart.CalculateHealth();
            Destroy(gameObject);
            if (playerheart.CurrentHealth > playerheart.Maxhealth)
            {
                playerheart.CurrentHealth = playerheart.Maxhealth;
            }
        }
    }
}
