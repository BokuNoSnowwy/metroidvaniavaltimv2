﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class bullet : MonoBehaviour
{
    private PlayerStats playerstats;
    public float speed;
    private Transform player;
    public float timeDestroy;
    private Vector2 target;

    // Start is called before the first frame update
    void Start()
    {
        playerstats = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        target = new Vector2(player.position.x, player.position.y);
      
      
    }

    // Update is called once per frame
    void Update()
    {
        // Permet au bullet d'aller vers la dernière position du joueur
       transform.position = Vector2.MoveTowards(transform.position, target, speed * Time.deltaTime);
      
        Destroy(gameObject, 0.9f);

    }

    public void OnTriggerEnter2D(Collider2D other)
    {//Inflige des dommage au player
        if (other.gameObject.CompareTag("Player"))
        {
            
            
                
            
            playerstats.TakeDamage(15);  
            if (other.transform.position.x > transform.position.x)
            {
                other.GetComponent<PlayerMovements>().knockbackCount = 0.1f;
                other.GetComponent<PlayerMovements>().knockbackRight = false;

            }
            else
            {
                other.GetComponent<PlayerMovements>().knockbackCount = 0.1f;
                other.GetComponent<PlayerMovements>().knockbackRight = true;
            }
            Destroy(gameObject);
          
        }
    }
}
    
