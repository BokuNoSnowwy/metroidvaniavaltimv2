﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class detectionplayer : MonoBehaviour

{    public float speed;
    public Transform player;
    public Patrolmonstre patrolmonstre;
    public GameObject monster;
    public bool followingPL;
    // Start is called before the first frame update
   public void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
     
    } 
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
         
       // patrolmonstre.anim.SetBool("following", true);
        
        followingPL = true;
       
        }
    }

    public void OnTriggerExit2D(Collider2D other)
    {// Exit l'aggro du monstre et ducoup il reprend sa patrouille
        if (other.gameObject.CompareTag("Player"))
        {
         
         followingPL = false;
         //  patrolmonstre.anim.SetBool("following", false);
        }
    }

    // Update is called once per frame
   public void Update()
   // Permet au monstre de suivre le player
    {Vector2 target = new Vector2(player.transform.position.x, 0);
        if (followingPL)
        {
            monster.transform.position =  Vector2.MoveTowards(transform.position, target,speed * Time.deltaTime);
        }
        
     
    }
}
