﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ghostvisible : MonoBehaviour

{     
    
    public SpriteRenderer ghostP;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    public void OnTriggerStay2D(Collider2D other)
    {// Rend visible le ghost
        if (other.gameObject.CompareTag("Player"))
        {
            ghostP.color = new Color(1f,1f,1f,1f);
        }
    }

    public void OnTriggerExit2D(Collider2D other)
    {// Rend le ghost transparent
        ghostP.color = new Color(1f,1f,1f, 0f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
