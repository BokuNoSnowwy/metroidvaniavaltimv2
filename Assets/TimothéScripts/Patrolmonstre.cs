﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Patrolmonstre : MonoBehaviour
{
    public float speed;
  
    public bool movingToright = true;
    public float distance;
    public Transform groundDétection;
    public bool knockback = false;
   

    public LayerMask layers;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (knockback == false)
        {
            transform.Translate(Vector2.right * speed * Time.deltaTime);


            RaycastHit2D groundInfo = Physics2D.Raycast(groundDétection.position, Vector2.right, distance, layers);
            RaycastHit2D groundleftinfo = Physics2D.Raycast(groundDétection.position, Vector2.left, distance, layers);
            if (groundInfo.collider == true)
            {
                if (movingToright == true)
                {
                    transform.eulerAngles = new Vector3(0f, -180f, 0f);
                    movingToright = false;
                }
                else if (groundleftinfo.collider == true && movingToright == false)
                {
                    
                    transform.eulerAngles = new Vector3(0f, 0f, 0f);
                    movingToright = true;
                }
            }
        }
        // Permet de faire bouger le monstre de droite a gauche 

           
    }
}
/* public void OnTriggerEnter2D(Collider2D other)
 {//  Sert à lancer l'anim following qui permet de suivre le player
     if (other.gameObject.CompareTag("Player"))
     {
         canMoove = false;
         anim.SetBool("following", true);

     }
 }

 public void OnTriggerExit2D(Collider2D other)
     {// Sert à exit l'anim et de reprendre sa patrouille au monstre
         if (other.gameObject.CompareTag("Player"))
         {
             canMoove = true;
             anim.SetBool("following", false);
         }
     }
 }*/

