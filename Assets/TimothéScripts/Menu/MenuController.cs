﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    public GameObject panel;
    public string sceneLoad, scenegameoverload;
    // Start is called before the first frame update
    void Start()
    {
        
    }
// Gere les différentes options du Menu et MenuPause
    public void Quitting()
    {
        Application.Quit();
        
    }

    public void Playmainscene()
    {
        SceneManager.LoadScene(sceneLoad);
    }

    public void setting()
    {
        panel.SetActive(true);
    }

    public void Resume()
    {
        Time.timeScale = 1f;
        panel.SetActive(false);
    }

    public void Retry()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(scenegameoverload);
    }


    public void Paneloptionoff()
    {
        panel.SetActive(false);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
