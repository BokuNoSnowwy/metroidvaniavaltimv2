﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Feather1Script : MonoBehaviour
{
    
    private int damage = 20;
    public Rigidbody2D rb;
    public float speed;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //rb.velocity = transform.forward * speed;
        if (transform.rotation.z >= 45)
        {
            transform.Translate(new Vector3(1f,-1f,1f) * Time.deltaTime * speed);
        }
        else
        {
            transform.Translate(new Vector3(-1f,-1f,1f) * Time.deltaTime * speed);
        }

    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        PlayerStats player = collision.GetComponent<PlayerStats>();
        if (player != null)
        {
            player.TakeDamage(damage);
            Destroy(gameObject);
        }
        if (collision.gameObject.layer == 8 || collision.gameObject.layer == 9)
        {
            Destroy(gameObject);
        }
    }
    
    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
