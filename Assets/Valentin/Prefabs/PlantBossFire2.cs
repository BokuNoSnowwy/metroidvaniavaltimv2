﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using DG.Tweening;
using UnityEngine;

public class PlantBossFire2 : MonoBehaviour
{ 
    private int damage = 20;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Instantiate());
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        PlayerStats player = collision.GetComponent<PlayerStats>();
        if (player != null)
        {
            player.TakeDamage(damage);
            Destroy(gameObject);
        }
    }
    
    public IEnumerator Instantiate()
    {
        Transform playerPos = GameObject.FindGameObjectWithTag("Player").transform;
        yield return new WaitForSeconds(1f);
        
        gameObject.transform.DOMove(playerPos.position, 1.3f).SetEase(Ease.Linear).OnComplete(()=>Destroy(gameObject));

    }
}