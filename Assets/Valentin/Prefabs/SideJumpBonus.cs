﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SideJumpBonus : MonoBehaviour
{
    public int healthpoint = 50;
    public float countdown = 5;
    private PlayerStats heal;
    private bool onDestroy = false;
    //public ParticleSystem particlePickUp;
    // Start is called before the first frame update
    void Start()
    {
        heal = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
    }

    // Update is called once per frame
    void Update()
    {
        if (onDestroy)
        {
            countdown -= Time.deltaTime;
            if (countdown == 0)
            {
                Destroy(gameObject);
            }
        }
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            FindObjectOfType<Audiomanager>().Play("HeartRecup");
          
                heal.CurrentHealth += healthpoint;
                heal.healthbar.value = heal.CalculateHealth();
                if (heal.CurrentHealth > heal.Maxhealth)
                {
                    heal.CurrentHealth = heal.Maxhealth;
                }
            

            PickUpPower(other);
           
        }
    }

    private void PickUpPower(Collider2D player)
    {

        //Instantiate(particlePickUp, transform.position, transform.rotation);
        player.GetComponent<PlayerPowers>().onPowerSideJump = true;
        StartCoroutine(GameObject.FindWithTag("InfoText").GetComponent<InfoTextDisplay>().DisplayPowerSideJump());

        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<BoxCollider2D>().enabled = false;
        GetComponent<CircleCollider2D>().enabled = false;
        
        Destroy(gameObject.GetComponentInChildren<ParticleSystem>());
        onDestroy = true;
    }
}
