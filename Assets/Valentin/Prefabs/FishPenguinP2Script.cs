﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishPenguinP2Script : MonoBehaviour
{
    private int damage = 20;
    public Rigidbody2D rb;

    public float minX;
    public float maxX;
    
    public float minY;
    public float maxY;

    
    // Start is called before the first frame update
    void Start()
    {
        float randomX = Random.Range(minX, maxX);
        float randomY = Random.Range(minY, maxY);

        int randomShot = Random.Range(0, 2);
        switch (randomShot)
        {
            case 0:
                transform.rotation = new Quaternion(transform.rotation.x,transform.rotation.y,-180f,transform.rotation.w);
                rb.AddForce(new Vector2(-randomX,randomY),ForceMode2D.Impulse);
                break;
                
            case 1 :
                rb.AddForce(new Vector2(randomX,randomY),ForceMode2D.Impulse);
                break;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.GetComponent<PlayerStats>().TakeDamage(damage);
            Destroy(gameObject);
        }

        if (collision.gameObject.layer == 8 || collision.gameObject.layer == 9)
        {
            Destroy(gameObject);
        }
    }


}
