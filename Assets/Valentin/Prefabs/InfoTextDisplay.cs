﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class InfoTextDisplay : MonoBehaviour
{
    private TextMeshProUGUI text;
    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public IEnumerator DisplayPowerSideJump()
    {
        text.text = "You can now do sideJumping on white walls"; 
        yield return new WaitForSeconds(5f);
        text.text = " "; 
    }
    
    public IEnumerator DisplayPowerBigSword()
    {
        text.text = "Press A to create a big sword that boost your damages and allow to destroy specifics walls"; 
        yield return new WaitForSeconds(5f);
        text.text = " "; 
    }
}
