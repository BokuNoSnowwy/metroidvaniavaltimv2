﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishPenguinScript : MonoBehaviour
{
    private int damage = 20;
    public Rigidbody2D rb;

    public float minX;
    public float maxX;
    
    public float minY;
    public float maxY;

    
    // Start is called before the first frame update
    void Start()
    {
        float randomX = Random.Range(minX, maxX);
        float randomY = Random.Range(minY, maxY);
        
        Debug.Log(randomX.ToString()+" "+randomY.ToString());
        if (transform.rotation.y < 0)
        {
            transform.rotation = new Quaternion(transform.rotation.x,transform.rotation.y,-180f,transform.rotation.w);
            rb.AddForce(new Vector2(-randomX,randomY),ForceMode2D.Impulse);
        }
        else
        {
            rb.AddForce(new Vector2(randomX,randomY),ForceMode2D.Impulse);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.GetComponent<PlayerStats>().TakeDamage(damage);
            Destroy(gameObject);
        }

        if (collision.gameObject.layer == 8 || collision.gameObject.layer == 9)
        {
            Destroy(gameObject);
        }
    }

}
