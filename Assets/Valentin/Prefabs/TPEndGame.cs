﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TPEndGame : MonoBehaviour
{
    public GameObject TimeManager;
    public TextMeshProUGUI infoText;
    public GameObject endGamePanel;
    private void Start()
    {
        infoText = GameObject.FindGameObjectWithTag("InfoText").GetComponent<TextMeshProUGUI>();
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            infoText.text = "Press E to TP";
            if (Input.GetKeyDown(KeyCode.E))
            {
                TimeManager.SetActive(false);
                Time.timeScale = 0f;
                endGamePanel.SetActive(true);
                
            }
            
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            infoText.text = "Press E to TP";
            if (Input.GetKeyDown(KeyCode.E))
            {
                TimeManager.SetActive(false);
                Time.timeScale = 0f;
                endGamePanel.SetActive(true);
            }
            
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            TimeManager.SetActive(true);
            infoText.text = "";
        }
    }
}
