﻿ using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

 public class PlayerPowers : MonoBehaviour
{
    public bool onPowerSideJump = false;
    public bool onPowerBigSword = false;

    private float rayDistance = 0.55f;
    public LayerMask layer;
    private PlayerMovements player;
    private SwordScript sword;

    public float timerBigSword = 10f;
    public float resetTimerBigSword = 10f;

    public float cooldownBigSword = 40f;
    public float resetCooldownBigSword = 40f;

    private RaycastHit2D raycastRight;
    private RaycastHit2D raycastLeft;

    private QTEManager qteManager;

    
    // Start is called before the first frame update
    void Start()
    {
        qteManager = GameObject.FindObjectOfType<QTEManager>();
        player = GetComponent<PlayerMovements>();
        sword = player.GetComponentInChildren<SwordScript>();
       
    }

    // Update is called once per frame
    void Update()
    {
        //Detect avec des raycast si le joueur est sur un wallJump 
        raycastRight = Physics2D.Raycast(transform.position, Vector2.right, rayDistance, layer);
        raycastLeft = Physics2D.Raycast(transform.position, Vector2.left, rayDistance, layer);
        
        //Si le joueur possède le pouvoir du wallJump
        if (onPowerSideJump)
        {
            if (raycastRight || raycastLeft)
            {
                if (Input.GetAxis("Horizontal") != 0)
                {
                    player.isOnSideWall = true;
                    player.animator.SetBool("iswall", true);
                }
            }
            else
            {
                player.isOnSideWall = false;
                player.animator.SetBool("iswall", false);
            }
        }
        
        //Si le joueur possède le pouvoir de la grande épée
        if (onPowerBigSword)
        {
            //Cooldown d'utilisation de l'épée
            if (cooldownBigSword >= 0)
            {
                cooldownBigSword -= Time.deltaTime;
            }

            if (cooldownBigSword <= 0)
            {
                if (!qteManager.onQte)
                {
                    //Si le joueur appuis sur A, son épée grandit, sa portée et son attaque augmente
                    if (Input.GetKeyDown(KeyCode.A))
                    {
                        sword.DisplayColliderAndSprite();
                        player.timerAttack = 1;
                        player.sword.transform.DOScale(1f, 1f)
                            .OnComplete(sword.HideColliderAndSprite);
                    
                        cooldownBigSword = resetCooldownBigSword;
                        sword.swordDmg = sword.bigSwordBaseDmg;
                        sword.onBigSword = true;
                    }
                }
                
            }

            //Gère le temps de grosse épée
            if (sword.onBigSword && onPowerBigSword)
            {
                if (timerBigSword >= 0)
                {
                    timerBigSword -= Time.deltaTime;
                }
                if (timerBigSword <= 0)
                {
                    player.timerAttack = 1;
                    sword.DisplayColliderAndSprite();
                    player.sword.transform.DOScale(0.5f, 0.5f)
                        .OnComplete(sword.HideColliderAndSprite);
                    sword.onBigSword = false;
                    sword.swordDmg = sword.swordBaseDmg;
                    timerBigSword = resetTimerBigSword;
                }
            }
        }
    }
}

