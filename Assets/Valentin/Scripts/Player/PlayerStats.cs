﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayerStats : MonoBehaviour
{

    public Rigidbody2D rgbd;
    public Animator animator;
    public float Maxhealth { get; set; }
    public  float  CurrentHealth { get; set; }
    public bool isDead = false;
    public bool isInvincible = false;
    public SwordScript playerSword;
    
    public Slider healthbar;
    public GameObject gameovePanel;
    private PlayerMovements player;
    private PlayerPowers playerPowers;


  
    // Start is called before the first frame update
    void Start()
    {
        playerPowers = GetComponent<PlayerPowers>();
        player = GetComponent<PlayerMovements>();
        playerSword = GetComponentInChildren<SwordScript>();
        Maxhealth = 150f;
        rgbd = gameObject.GetComponent<Rigidbody2D>();
        CurrentHealth = Maxhealth;
        healthbar.value = CalculateHealth();

    }

    // Update is called once per frame
    void Update()
    {
        if (isDead)
        {//Appel le menu Gameover et freez le temps
           
            animator.SetBool("death", true);
            Time.timeScale = 0f;
            gameovePanel.SetActive(true);
            FindObjectOfType<Audiomanager>().Stop("MainAudio");
            FindObjectOfType<Audiomanager>().Stop("MusicMain2");
            FindObjectOfType<Audiomanager>().Stop("MusicMain3");
           
        }
    }



    public void TakeDamage(int damage)
    { // inflige des dommages au player si il n'est pas invincible
        if (!isDead)
        { 
          
            if (!isInvincible)
            {
                FindObjectOfType<Audiomanager>().Play("PlayerHit");
                CurrentHealth-= damage;
                healthbar.value = CalculateHealth();
                playerSword.ResetChainQTE();
                StartCoroutine(DmgInvincibility());
                if (CurrentHealth <= 0)
                {
                    Die();
                }
            }
        }
    }

    public float CalculateHealth()
    {// Perme de mettre a jour le slider

        return CurrentHealth / Maxhealth;
    }

    
    public IEnumerator DmgInvincibility()
    {
        isInvincible = true;
        yield return new WaitForSeconds(2f); // waits 2 seconds
        isInvincible = false;
    }

    public void Die()
    {
        playerPowers.cooldownBigSword = 0;
        playerSword.onBigSword = false;
        isDead = true;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Monster") || other.gameObject.name == "MarchingBoss")
        {
            //Si le monstre peut infliger des dégats
            if (other.gameObject.GetComponentInParent<monsterScript>().canInflictDmg)
            {
                //Active le knockBack du joueur
                if (other.transform.position.x > transform.position.x)
                {
                    if (!isInvincible)
                    {
                        player.knockbackCount = 0.1f;
                        player.knockbackRight = true;
                    }
                }
                else
                {
                    if (!isInvincible)
                    {
                        player.knockbackCount = 0.1f;
                        player.knockbackRight = false;
                    }
                }
                TakeDamage(other.collider.GetComponentInParent<monsterScript>().damage);
            }
        }
        else if (other.gameObject.CompareTag("pikes"))
        {// Permet au pikes d'infliger des dégats et de pousser le joueur vers le haut
            if (!isInvincible)
            {
                player.knockbackCount = 0.1f;
                player.knockbackpikes = true;
                TakeDamage(20);
            }
            else
            {
                player.knockbackCount = 0.1f;
                player.knockbackpikes = false;
            }
        }
    }

    private void OnCollisionStay2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Monster") || other.gameObject.name == "MarchingBoss")
        {
            //Si le monstre peut infliger des dégats
            if (other.gameObject.GetComponentInParent<monsterScript>().canInflictDmg)
            {
                if (other.transform.position.x > transform.position.x)
                {
                    //Active le knockBack du joueur
                    if (!isInvincible)
                    {
                        player.knockbackCount = 0.1f;
                        player.knockbackRight = true;
                    }
                }
                else
                {
                    if (!isInvincible)
                    {
                        player.knockbackCount = 0.1f;
                        player.knockbackRight = false;
                    }
                }
                TakeDamage(other.collider.GetComponentInParent<monsterScript>().damage);
            }
        }
        else if (other.gameObject.CompareTag("pikes"))
        {// Permet au pikes d'infliger des dégats et de pousser le joueur vers le haut
            if (!isInvincible)
            {
                player.knockbackCount = 0.1f;
                player.knockbackpikes = true;
                TakeDamage(20);
            }
            else
            {
                player.knockbackCount = 0.1f;
                player.knockbackpikes = false;
            }
        }
    }
}
