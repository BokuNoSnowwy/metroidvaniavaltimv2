﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TpCollider : MonoBehaviour
{
    public TextMeshProUGUI infoText;
    public int index;
    public TpScript tpManager;

    public bool activated;
    // Start is called before the first frame update
    void Start()
    {
        infoText = GameObject.FindGameObjectWithTag("InfoText").GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    private void OnTriggerStay2D(Collider2D other)
    {
        //Si le joueur est proche d'un TP, un message disant d'appuyer sur E apparait
        if (other.CompareTag("Player"))
        {
            activated = true;
            tpManager.currentIndex = index;
            infoText.text = "Press E to TP";
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (tpManager.onTp)
                {
                    tpManager.onTp = false;
                }
                else
                {
                    tpManager.onTp = true;
                }
                
            }
            
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //Si le joueur est proche d'un TP, un message disant d'appuyer sur E apparait
        if (other.CompareTag("Player"))
        {
            activated = true;
            tpManager.currentIndex = index;
            infoText.text = "Press E to TP";
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (tpManager.onTp)
                {
                    tpManager.onTp = false;
                }
                else
                {
                    tpManager.onTp = true;
                }
                
            }
            
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            infoText.text = "";
            tpManager.onTp = false;
        }
    }

}
