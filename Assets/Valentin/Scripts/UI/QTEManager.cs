﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class QTEManager : MonoBehaviour
{
    public monsterScript monster;
    public TimeManagerScript timeManager;
    public SwordScript playerSword;
    
    public bool onQte = false;
    public TextMeshProUGUI textQTE;
    public bool qteIsDelayed = false;
    
    public int indexList;
    public List<string> currentList = new List<string>();

    public float timerQte = 2;
    public float resetTimeQte = 2;
    
    // Start is called before the first frame update
    void Start()
    {
        playerSword = GameObject.FindObjectOfType<SwordScript>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (onQte)
        {
            //Timer du QTE qui n'est pas affecté par le ralentissement du temps.
            if (timerQte >= 0)
                {
                    timerQte -= Time.unscaledDeltaTime;
                }
            //Si le timer arrive a expiration, désactive le qte
            if (timerQte <= 0)
            {
                monster.RefreshList(monster.initialKey);
                DesactivateQTE(monster);
                timerQte = 2;
                onQte = false;
            }
           
            //Si un monstre est focused pas le qte, detecte l'input des touches
            if (monster)
            {
                if (currentList.Count >= 1)
                {
                    if (Input.GetKeyDown(KeyCode.A))
                    {
                        //Success
                        if (currentList[0].ToUpper() == "A")
                        {
                            currentList.RemoveAt(indexList);
                            RefreshListTextMesh(currentList);
                        }
                        //Fail
                        else
                        {
                            DesactivateQTE(monster);
                            onQte = false;
                        }
                    }
                
                    if (Input.GetKeyDown(KeyCode.Z))
                    {
                        //Success
                        if (currentList[0].ToUpper() == "Z")
                        {
                            currentList.RemoveAt(indexList);
                            //Met à jour le text mesh
                            RefreshListTextMesh(currentList);
                        }
                        //Fail
                        else
                        {
                            DesactivateQTE(monster);
                            onQte = false;
                        }
                    }
                    
                    if (Input.GetKeyDown(KeyCode.E))
                    {
                        //Success
                        if (currentList[0].ToUpper() == "E")
                        {
                            currentList.RemoveAt(indexList);
                            RefreshListTextMesh(currentList);
                        }
                        //Fail
                        else
                        {
                            DesactivateQTE(monster);
                            onQte = false;
                        }
                    }
                }
                //Si la liste du QTE est vide (réussi) désactive le QTE et inflige des dégats au monstre (detection de la liste vide)
                else if (currentList.Count <= 0)
                {
                    DesactivateQTE(monster);
                }
                if (!textQTE.IsActive())
                {
                    textQTE.enabled = true;
                }
            }
        }
        else
        {
            timerQte = resetTimeQte;
            textQTE.enabled = false;
        }
    }

    //Lance le QTE grâce au monstre en parametre
    public void ActivateQTE(monsterScript monsterDamaged)
    {
        if (!onQte)
        {
            monster = monsterDamaged;
            
            onQte = true;
            currentList = monsterDamaged.qteList;
            indexList = 0;
            RefreshListTextMesh(currentList);
        }
    }
    
    //Quand le monstre est mort ou quand on rate le cte ou bien on prend des dégats, le qte est désactivé
    public void DesactivateQTE(monsterScript monsterDamaged)
    {
        if (onQte)
        {
            //Si le qte est réussi (liste vide) inflige des dégats au monstre, augmente la chaine de dégat de l'épée et augmente
            //le nombre d'input du prochain QTE
            if (currentList.Count < 1)
            {
                monsterDamaged.nbKey += 1;
                monsterDamaged.RefreshList(monsterDamaged.nbKey);
                monsterDamaged.TakeDamage(playerSword.swordDmg * (1+playerSword.chainQTE*0.1f),playerSword.player.transform);
                playerSword.chainQTE += 1;
                timeManager.ReturnNormalTime();
            }
            else
            {
                monsterDamaged.RefreshList(monsterDamaged.initialKey);
                playerSword.chainQTE = 1;
            }
        }
        playerSword.inMonster = false;
        textQTE.text = "";
        onQte = false;
        currentList = null;
        monster = null;
        timeManager.ReturnNormalTime();

    }

    //Refresh l'affichage du QTE (si on appuis sur A sur le Qte, la lettre A disparait directement)
    public void RefreshListTextMesh(List<string> list)
    {
        textQTE.text = "";
        foreach (var element in list)
        {
            textQTE.text += element;
        }
    }
}
