﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingBossDetection : MonoBehaviour
{
    public GameObject tileMapWall;
    public Transform dropSpot;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    //Quand le joueur entre dans la zone d'aggro du boss, ce dernier se déclanche et ferme l'entrée
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            GetComponentInChildren<FlyingBossScript>().awake = true;
            GetComponentInChildren<FlyingBossScript>().player = other.gameObject;
            tileMapWall.SetActive(true);


        }
    }
}
