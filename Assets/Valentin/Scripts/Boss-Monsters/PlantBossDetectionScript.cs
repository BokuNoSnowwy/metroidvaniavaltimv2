﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantBossDetectionScript : MonoBehaviour
{
    public PlantBossScript bossScript;
    public GameObject tileMapWall;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    //Quand le joueur entre dans la zone d'aggro du boss, ce dernier se déclanche et ferme l'entrée
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            bossScript.awake = true;
            bossScript.player = other.gameObject;
            tileMapWall.SetActive(true);

        }
    }
}
