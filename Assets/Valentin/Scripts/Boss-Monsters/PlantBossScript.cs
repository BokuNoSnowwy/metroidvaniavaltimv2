﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlantBossScript : MonoBehaviour
{
    public bool awake = false;
    public bool p2 = false;
    public monsterScript stats;
    public float baseHealth;
    public GameObject player;

    public GameObject firePrefab1;
    public GameObject firePrefab2;

    public Transform[] fireSpots;

    public float timerAttack = 3;
    public float timerAttackReset = 3;

    public GameObject bonusLoot;
    
    //Animator
    public Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        stats = GetComponent<monsterScript>();
        baseHealth = stats.health;
    }

    // Update is called once per frame
    void Update()
    {
        //Si le boss a 50% de vie ou moins, déclanche la p2
        if (stats.health <= baseHealth / 2)
        {
            p2 = true;
            animator.SetBool("MidLife",true);
            timerAttackReset = 2;
        }
        
        if (awake)
        {
            animator.SetBool("Awake", true);
            //Timer d'attaque
            //Si le monstre est en p1, il va attaqué depuis soit sa droite, soit sa gauche
            if (timerAttack >= 0)
            {
                timerAttack -= Time.deltaTime;        
            }
            if (timerAttack <= 0)
            {
                //Si le boss est en p2, utilise forcément la deuxieme attaque
                if (p2)
                {
                    Attack2();
                }
                else
                {
                    int attack = Random.Range(0, 2);
                    switch (attack)
                    {
                        case 0 :
                            Attack1(fireSpots[0].transform);
                            break;
                        case 1 :
                            Attack1(fireSpots[1].transform);
                            break;
                        default:
                            break;
                    }
                }
               
                timerAttack = timerAttackReset;
            }
        }

        if (stats.health <= 0)
        {
            //Si le boss meurt, il drop un objet donnant lieu à un nouveau pouvoir
            //Désactive le mur fermée de l'entrée et change la musique
            FindObjectOfType<Audiomanager>().Stop("MainAudio");
            FindObjectOfType<Audiomanager>().Play("MusicMain2");
            Instantiate(bonusLoot, transform.position,Quaternion.identity);
            gameObject.SetActive(false);
            GetComponentInChildren<PlantBossDetectionScript>().tileMapWall.SetActive(false);
        }
    }

    //Lance des boules jaunes depuis sa droite ou sa gauche
    public void Attack1(Transform spot)
    {
        for (int i = 0; i < 6; i++)
        {
            float inter = 36f;
            Instantiate(firePrefab1, spot.position, Quaternion.Euler(spot.rotation.x, spot.rotation.y, 90f + inter * i));
        }
    }

    //Crée deux boules rouge a le droite et la gauche du joueur, et les fait se rapprocher vers le joueur
    public void Attack2()
    {
        Instantiate(firePrefab2, new Vector3(player.transform.position.x + 4.5f,player.transform.position.y-0.5f,player.transform.position.z), Quaternion.Euler(new Vector3(0f,0f,0f)));
        Instantiate(firePrefab2, new Vector3(player.transform.position.x - 4.5f,player.transform.position.y-0.5f,player.transform.position.z), Quaternion.Euler(new Vector3(0f,180f,0f)));
    }

}
