﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class PenguinBossScript : MonoBehaviour
{
    //Animator
    public Animator animator;
    
    // Start is called before the first frame update
    public GameObject player;
    private float speed = 3f;
    public monsterScript stats;
    public float baseHealth;

    public bool movingRight = true;
    public float distanceBetweenPlayer;
    
    //jump
    public bool isGrounded;
    public float rayDistance;
    public LayerMask layers;

    public bool awake;

    public bool stopMoving;
    
    //attack
    public bool onAttack = false;
    public bool onAttack1 = false;
    public bool onAttack2 = false;
    
    public float timerAttack = 6;
    public float timerAttackReset = 6;
    
    public float timerAttack2 = 0.3f;
    public GameObject prefabFish;
    public GameObject prefabFishP2;
    public float jumpForce;

    public bool p2;
    
    //Death portal
    public GameObject endGameTp;
    
    // Start is called before the first frame update
    void Start()
    {
        stats = GetComponent<monsterScript>();
        baseHealth = stats.health;
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        //Vérifie si le boss touche le sol
        if (Physics2D.Raycast(transform.position, Vector2.down, rayDistance, layers))
        {
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
        }
        
        if (awake)
        {
            if (!stopMoving)
            {
                //Le boss va naturelement aller sur le joueur
                animator.SetBool("walk", true);
                if (isGrounded)
                {
                    //Vérifie si le boss est proche du player
                    if (System.Math.Abs(transform.position.x - player.transform.position.x) < 50f && System.Math.Abs(transform.position.x - player.transform.position.x) > distanceBetweenPlayer)
                    {
                        transform.Translate(Vector2.right * speed * Time.deltaTime);

                        //Si il est proche le pourchasse
                        if (transform.position.x >= player.transform.position.x)
                            //Quand le joueur passe par dessus le monstre, ce dernier change de sens pour le suivre
                            transform.eulerAngles = new Vector3(0f, -180, 0f);
                        else
                        {
                            transform.eulerAngles = new Vector3(0f, 0, 0f);
                        }
                    }
                    //Si le boss est trop près, il s'arrète
                    else if (System.Math.Abs(transform.position.x - player.transform.position.x) <= distanceBetweenPlayer)
                    {

                    }
                }
                else
                {
                    //Quand il saute, il a un déplacement fixe, il ne changera pas de sens en plein saut si il dépasse le player
                    transform.Translate(Vector2.right * speed * Time.deltaTime);
                }
            }
            else
            {
                if (transform.position.x >= player.transform.position.x)
                    //Quand le joueur passe par dessus le monstre, ce dernier change de sens pour le suivre du regard
                    transform.eulerAngles = new Vector3(0f, -180, 0f);
                else
                {
                    transform.eulerAngles = new Vector3(0f, 0, 0f);
                }
            }
            

            //ATTACK1
            if (!onAttack)
            {
                if (timerAttack >= 0)
                {
                    timerAttack -= Time.deltaTime;        
                }

                if (timerAttack <= 0)
                {
                    int attack;
                    if (!p2)
                    {
                        attack =  Random.Range(0, 2);
                    }
                    else
                    {
                        attack = 2;
                    }

                    switch (attack)
                    {
                        case 0 :
                            StartCoroutine(Attack1Duration());
                            break;
                        case 1 :
                            StartCoroutine(Attack2Duration());
                            break;
                        case 2 :
                            StartCoroutine(Attack3Duration());
                            break;
                        default:
                            break;
                    }
                    timerAttack = timerAttackReset;
                }
            }
            
            
            if (onAttack1)
            {
                //Le boss saute
                animator.SetBool("walk", false);
                animator.SetBool("shoot", false);
                animator.SetBool("jump",true);
                if (isGrounded)
                {
                    if (!p2)
                    {
                        GetComponent<Rigidbody2D>().velocity = Vector2.up * jumpForce;
                    }
                    else
                    {
                        //Si il est en p2, saute plus haut et plus rapidement
                        GetComponent<Rigidbody2D>().velocity = Vector2.up * jumpForce*2.5f;
                    }
                }
            }
            else
            {
                animator.SetBool("jump",false);
            }

            if (onAttack2)
            {
                //Le boss va tirer des poissons, si il est en p2, tir des poissons différement
                if (!p2)
                {
                    animator.SetBool("walk", false);
                    animator.SetBool("shoot", true);
                    animator.SetBool("jump",false);
                    if (timerAttack2 >= 0)
                    {
                        timerAttack2 -= Time.deltaTime;
                    }
                    if (timerAttack2 <= 0)
                    {
                        Attack2();
                        timerAttack2 =  Random.Range(0.4f,0.6f);
                    }
                }
                else
                {
                    if (timerAttack2 >= 0)
                    {
                        timerAttack2 -= Time.deltaTime;
                    }
                    if (timerAttack2 <= 0)
                    {
                        Attack3();
                        timerAttack2 =  0.7f;
                    }
                }
            }
            else
            {
                animator.SetBool("shoot", false);
            }
            
            //Si le boss meurt, fait pop le tp de fin de jeu
            if (stats.health <= 0)
            {
                gameObject.SetActive(false);
                endGameTp.SetActive(true);
                GetComponentInChildren<PenguinBossDetection>().tileMapWall.SetActive(false);
            }

            //Si le boss a 50% de vie ou moins, déclanche la p2
            if (stats.health <= baseHealth / 2)
            {
                if (!p2)
                {
                    if (isGrounded)
                    {
                        StartCoroutine(ActivateP2());
                    }
                }
            }
            
        }
    }

    public void Attack2()
    {
        Instantiate(prefabFish, transform.position, transform.rotation);
    }
    
    public void Attack3()
    {
        Instantiate(prefabFishP2, transform.position, transform.rotation);
    }
    
    public IEnumerator Attack1Duration()
    {
        onAttack = true;
        onAttack1 = true;
        yield return new WaitForSeconds(3.5f);
        onAttack1 = false;
        onAttack = false;
    }

    public IEnumerator Attack2Duration()
    {
        onAttack = true;
        stopMoving = true;
        yield return new WaitForSeconds(1f);
        onAttack2 = true;
        yield return new WaitForSeconds(3.5f);
        stopMoving = false;
        onAttack2 = false;
        onAttack = false;
    }
    
    public IEnumerator Attack3Duration()
    {
        onAttack = true;
        onAttack1 = true;
        onAttack2 = true;
        yield return new WaitForSeconds(3.5f);
        onAttack1 = false;
        onAttack2 = false;
        onAttack = false;
    }
    

    public IEnumerator ActivateP2()
    {
        //Feedback visuel pour la p2
        transform.DOShakePosition(1, 0.1f);
        GetComponent<Rigidbody2D>().gravityScale = 5;
        yield return new WaitForSeconds(1f);
        p2 = true;
    }
}