﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
using DG.Tweening;
using UnityEngine.Experimental.PlayerLoop;


public class monsterScript : MonoBehaviour
{
    
   // public BoxCollider2D boxCollider;
   // public CapsuleCollider2D capsuleCollider1;
   // public CapsuleCollider2D capsuleCollider2;
   
   public List<string> qteList = new List<string>();
    public int nbKey;
    public int initialKey;
    public string stringElement = "uio";

    public float health;
    public int damage;
    public bool canInflictDmg = true;

    private bool isAddforce;
    private GameObject player;
    public Rigidbody2D rgbd;

    public float knockback = 3;
    private float knockbackLenght;
    public float knockbackCount;

    private Patrolmonstre monster;
    public FloatingNumbers dmgText;
    public GameObject prefabHeart;

    // Start is called before the first frame update
    void Start()
    {
        rgbd = GetComponent<Rigidbody2D>();
        dmgText = GameObject.FindWithTag("DmgText").GetComponent<FloatingNumbers>();
        player = GameObject.FindGameObjectWithTag("Player");
        if (GetComponent<Patrolmonstre>())
        {
            monster = GetComponent<Patrolmonstre>();
        }
        
        initialKey = nbKey;
        for (int i = 0; i < nbKey; i++)
        {
            int random = Random.Range(0, stringElement.Length);
            qteList.Add(stringElement[random].ToString());
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Si le monstre n'est pas un boss. on lui ajoute du knockback
        if (gameObject.tag == "Monster")
        {
            //Le monstre peut infliger des dmg si il n'est pas en état de knockback
            if (knockbackCount <= 0)
            {
                canInflictDmg = true;
                if (monster)
                {
                    monster.knockback = false;
                }
            }
            else
            {
                canInflictDmg = false;
                //empeche le monstre d'infliger des dgts et le projette par rapport a la position du joueur
                if ((player.transform.position.x - gameObject.transform.position.x) >= 0f)
                {
                    rgbd.velocity = new Vector2(-knockback,0);
                }
                else
                {
                    rgbd.velocity = new Vector2(knockback,0);
                }

                if (monster)
                {
                    monster.knockback = true;
                }
                knockbackCount -= Time.deltaTime;
            }
        }
    }
    
    //Refresh la liste du QTE du monstre
    public void RefreshList(int newNbKey)
    {
        //Debug.Log("RefreshList");
        nbKey = newNbKey;
        qteList.Clear();
        for (int i = 0; i < newNbKey; i++)
        {
            int random = Random.Range(0, stringElement.Length);
            qteList.Add(stringElement[random].ToString());
        }
    }

    //Inflige des dégats au monstre
    public void TakeDamage(float damage,Transform playerPosition)
    {
        if (dmgText)
        {
            dmgText.damageNumber = (int) damage;
            dmgText.displayDamage = true;
        }
        
        if (gameObject.CompareTag("Boss"))
        {
            if (GetComponent<FlyingBossScript>())
            {
                StartCoroutine(GetComponent<FlyingBossScript>().DamagedAnimation());
            }
        }
        
        health -= damage;
        FeedBackDamage();
        knockbackCount = 0.75f;
        if (health <= 0)
        {
            FindObjectOfType<Audiomanager>().Play("MonsterDie");
            //Si un monstre meurt, il a 25% de chance de drop un coeur
            if (gameObject.CompareTag("Monster"))
            {
                int random = Random.Range(1, 5);
                if (random == 2)
                {
                    Instantiate(prefabHeart, transform.position, Quaternion.identity);
                }
                gameObject.SetActive(false);
            }
        }
    }

    //Quand un QTE est réalisé, désactive tous les collider du monstre pendant 1s (empechant le proc d'un nouveau QTE sur un meme coup)
    public IEnumerator WaitForQTE()
    {
        foreach (var VARIABLE in GetComponents<Collider2D>())
        {
            VARIABLE.enabled = false;
        }
        yield return new WaitForSeconds(1f);
        foreach (var VARIABLE in GetComponents<Collider2D>())
        {
            VARIABLE.enabled = true;
        }
    }

    public void FeedBackDamage()
    {
        GetComponent<SpriteRenderer>().DOColor(Color.red, 0.1f)
            .OnComplete(()=>GetComponent<SpriteRenderer>().DOColor(Color.white, 0.1f)
                .OnComplete(()=>GetComponent<SpriteRenderer>().DOColor(Color.red, 0.1f)
                .OnComplete(()=>GetComponent<SpriteRenderer>().DOColor(Color.white, 0.1f)
                    .OnComplete(()=>GetComponent<SpriteRenderer>().DOColor(Color.red, 0.1f)
                        .OnComplete(()=>GetComponent<SpriteRenderer>().DOColor(Color.white, 0.1f))))));
    }
}
